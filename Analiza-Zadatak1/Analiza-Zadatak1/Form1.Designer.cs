﻿namespace Analiza_Zadatak1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonnula = new System.Windows.Forms.Button();
            this.buttonplus = new System.Windows.Forms.Button();
            this.buttondevet = new System.Windows.Forms.Button();
            this.buttonosam = new System.Windows.Forms.Button();
            this.buttonsedam = new System.Windows.Forms.Button();
            this.buttonsest = new System.Windows.Forms.Button();
            this.buttonpet = new System.Windows.Forms.Button();
            this.buttoncetiri = new System.Windows.Forms.Button();
            this.buttontri = new System.Windows.Forms.Button();
            this.buttondva = new System.Windows.Forms.Button();
            this.buttonjedan = new System.Windows.Forms.Button();
            this.buttoncosinus = new System.Windows.Forms.Button();
            this.buttonsinus = new System.Windows.Forms.Button();
            this.buttonmnozi = new System.Windows.Forms.Button();
            this.buttonminus = new System.Windows.Forms.Button();
            this.buttondijeli = new System.Windows.Forms.Button();
            this.buttonsqrt = new System.Windows.Forms.Button();
            this.buttonlog = new System.Windows.Forms.Button();
            this.buttonpower = new System.Windows.Forms.Button();
            this.textbox = new System.Windows.Forms.TextBox();
            this.buttontocka = new System.Windows.Forms.Button();
            this.buttonce = new System.Windows.Forms.Button();
            this.buttonizlaz = new System.Windows.Forms.Button();
            this.buttonjednako = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonnula
            // 
            this.buttonnula.Location = new System.Drawing.Point(111, 366);
            this.buttonnula.Name = "buttonnula";
            this.buttonnula.Size = new System.Drawing.Size(64, 58);
            this.buttonnula.TabIndex = 0;
            this.buttonnula.Text = "0";
            this.buttonnula.UseVisualStyleBackColor = true;
            this.buttonnula.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonplus
            // 
            this.buttonplus.Location = new System.Drawing.Point(281, 147);
            this.buttonplus.Name = "buttonplus";
            this.buttonplus.Size = new System.Drawing.Size(63, 56);
            this.buttonplus.TabIndex = 1;
            this.buttonplus.Text = "+";
            this.buttonplus.UseVisualStyleBackColor = true;
            this.buttonplus.Click += new System.EventHandler(this.buttonplus_Click);
            // 
            // buttondevet
            // 
            this.buttondevet.Location = new System.Drawing.Point(196, 146);
            this.buttondevet.Name = "buttondevet";
            this.buttondevet.Size = new System.Drawing.Size(66, 56);
            this.buttondevet.TabIndex = 2;
            this.buttondevet.Text = "9";
            this.buttondevet.UseVisualStyleBackColor = true;
            this.buttondevet.Click += new System.EventHandler(this.buttondevet_Click);
            // 
            // buttonosam
            // 
            this.buttonosam.Location = new System.Drawing.Point(111, 146);
            this.buttonosam.Name = "buttonosam";
            this.buttonosam.Size = new System.Drawing.Size(64, 56);
            this.buttonosam.TabIndex = 3;
            this.buttonosam.Text = "8";
            this.buttonosam.UseVisualStyleBackColor = true;
            this.buttonosam.Click += new System.EventHandler(this.buttonosam_Click);
            // 
            // buttonsedam
            // 
            this.buttonsedam.Location = new System.Drawing.Point(24, 146);
            this.buttonsedam.Name = "buttonsedam";
            this.buttonsedam.Size = new System.Drawing.Size(67, 56);
            this.buttonsedam.TabIndex = 4;
            this.buttonsedam.Text = "7";
            this.buttonsedam.UseVisualStyleBackColor = true;
            this.buttonsedam.Click += new System.EventHandler(this.buttonsedam_Click);
            // 
            // buttonsest
            // 
            this.buttonsest.Location = new System.Drawing.Point(196, 217);
            this.buttonsest.Name = "buttonsest";
            this.buttonsest.Size = new System.Drawing.Size(66, 57);
            this.buttonsest.TabIndex = 5;
            this.buttonsest.Text = "6";
            this.buttonsest.UseVisualStyleBackColor = true;
            this.buttonsest.Click += new System.EventHandler(this.buttonsest_Click);
            // 
            // buttonpet
            // 
            this.buttonpet.Location = new System.Drawing.Point(111, 217);
            this.buttonpet.Name = "buttonpet";
            this.buttonpet.Size = new System.Drawing.Size(64, 57);
            this.buttonpet.TabIndex = 6;
            this.buttonpet.Text = "5";
            this.buttonpet.UseVisualStyleBackColor = true;
            this.buttonpet.Click += new System.EventHandler(this.buttonpet_Click);
            // 
            // buttoncetiri
            // 
            this.buttoncetiri.Location = new System.Drawing.Point(24, 217);
            this.buttoncetiri.Name = "buttoncetiri";
            this.buttoncetiri.Size = new System.Drawing.Size(67, 57);
            this.buttoncetiri.TabIndex = 7;
            this.buttoncetiri.Text = "4";
            this.buttoncetiri.UseVisualStyleBackColor = true;
            this.buttoncetiri.Click += new System.EventHandler(this.buttoncetiri_Click);
            // 
            // buttontri
            // 
            this.buttontri.Location = new System.Drawing.Point(196, 289);
            this.buttontri.Name = "buttontri";
            this.buttontri.Size = new System.Drawing.Size(66, 59);
            this.buttontri.TabIndex = 8;
            this.buttontri.Text = "3";
            this.buttontri.UseVisualStyleBackColor = true;
            this.buttontri.Click += new System.EventHandler(this.buttontri_Click);
            // 
            // buttondva
            // 
            this.buttondva.Location = new System.Drawing.Point(111, 289);
            this.buttondva.Name = "buttondva";
            this.buttondva.Size = new System.Drawing.Size(64, 59);
            this.buttondva.TabIndex = 9;
            this.buttondva.Text = "2";
            this.buttondva.UseVisualStyleBackColor = true;
            this.buttondva.Click += new System.EventHandler(this.buttondva_Click);
            // 
            // buttonjedan
            // 
            this.buttonjedan.Location = new System.Drawing.Point(24, 289);
            this.buttonjedan.Name = "buttonjedan";
            this.buttonjedan.Size = new System.Drawing.Size(67, 59);
            this.buttonjedan.TabIndex = 10;
            this.buttonjedan.Text = "1";
            this.buttonjedan.UseVisualStyleBackColor = true;
            this.buttonjedan.Click += new System.EventHandler(this.button11_Click);
            // 
            // buttoncosinus
            // 
            this.buttoncosinus.Location = new System.Drawing.Point(111, 99);
            this.buttoncosinus.Name = "buttoncosinus";
            this.buttoncosinus.Size = new System.Drawing.Size(64, 41);
            this.buttoncosinus.TabIndex = 11;
            this.buttoncosinus.Text = "cos";
            this.buttoncosinus.UseVisualStyleBackColor = true;
            this.buttoncosinus.Click += new System.EventHandler(this.buttoncosinus_Click);
            // 
            // buttonsinus
            // 
            this.buttonsinus.Location = new System.Drawing.Point(24, 99);
            this.buttonsinus.Name = "buttonsinus";
            this.buttonsinus.Size = new System.Drawing.Size(67, 41);
            this.buttonsinus.TabIndex = 12;
            this.buttonsinus.Text = "sin";
            this.buttonsinus.UseVisualStyleBackColor = true;
            this.buttonsinus.Click += new System.EventHandler(this.buttonsinus_Click);
            // 
            // buttonmnozi
            // 
            this.buttonmnozi.Location = new System.Drawing.Point(281, 215);
            this.buttonmnozi.Name = "buttonmnozi";
            this.buttonmnozi.Size = new System.Drawing.Size(63, 59);
            this.buttonmnozi.TabIndex = 13;
            this.buttonmnozi.Text = "*";
            this.buttonmnozi.UseVisualStyleBackColor = true;
            this.buttonmnozi.Click += new System.EventHandler(this.buttonmnozi_Click);
            // 
            // buttonminus
            // 
            this.buttonminus.Location = new System.Drawing.Point(354, 146);
            this.buttonminus.Name = "buttonminus";
            this.buttonminus.Size = new System.Drawing.Size(63, 57);
            this.buttonminus.TabIndex = 14;
            this.buttonminus.Text = "-";
            this.buttonminus.UseVisualStyleBackColor = true;
            this.buttonminus.Click += new System.EventHandler(this.buttonminus_Click);
            // 
            // buttondijeli
            // 
            this.buttondijeli.Location = new System.Drawing.Point(354, 215);
            this.buttondijeli.Name = "buttondijeli";
            this.buttondijeli.Size = new System.Drawing.Size(63, 59);
            this.buttondijeli.TabIndex = 15;
            this.buttondijeli.Text = "/";
            this.buttondijeli.UseVisualStyleBackColor = true;
            this.buttondijeli.Click += new System.EventHandler(this.buttondijeli_Click);
            // 
            // buttonsqrt
            // 
            this.buttonsqrt.Location = new System.Drawing.Point(281, 99);
            this.buttonsqrt.Name = "buttonsqrt";
            this.buttonsqrt.Size = new System.Drawing.Size(63, 41);
            this.buttonsqrt.TabIndex = 16;
            this.buttonsqrt.Text = "sqrt";
            this.buttonsqrt.UseVisualStyleBackColor = true;
            this.buttonsqrt.Click += new System.EventHandler(this.buttonsqrt_Click);
            // 
            // buttonlog
            // 
            this.buttonlog.Location = new System.Drawing.Point(196, 99);
            this.buttonlog.Name = "buttonlog";
            this.buttonlog.Size = new System.Drawing.Size(66, 41);
            this.buttonlog.TabIndex = 17;
            this.buttonlog.Text = "log";
            this.buttonlog.UseVisualStyleBackColor = true;
            this.buttonlog.Click += new System.EventHandler(this.buttonlog_Click);
            // 
            // buttonpower
            // 
            this.buttonpower.Location = new System.Drawing.Point(350, 99);
            this.buttonpower.Name = "buttonpower";
            this.buttonpower.Size = new System.Drawing.Size(67, 41);
            this.buttonpower.TabIndex = 18;
            this.buttonpower.Text = "^2";
            this.buttonpower.UseVisualStyleBackColor = true;
            this.buttonpower.Click += new System.EventHandler(this.buttonpower_Click);
            // 
            // textbox
            // 
            this.textbox.Location = new System.Drawing.Point(24, 57);
            this.textbox.Name = "textbox";
            this.textbox.Size = new System.Drawing.Size(393, 22);
            this.textbox.TabIndex = 19;
            this.textbox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // buttontocka
            // 
            this.buttontocka.Location = new System.Drawing.Point(196, 366);
            this.buttontocka.Name = "buttontocka";
            this.buttontocka.Size = new System.Drawing.Size(66, 58);
            this.buttontocka.TabIndex = 20;
            this.buttontocka.Text = ".";
            this.buttontocka.UseVisualStyleBackColor = true;
            this.buttontocka.Click += new System.EventHandler(this.buttontocka_Click);
            // 
            // buttonce
            // 
            this.buttonce.Location = new System.Drawing.Point(24, 366);
            this.buttonce.Name = "buttonce";
            this.buttonce.Size = new System.Drawing.Size(67, 58);
            this.buttonce.TabIndex = 21;
            this.buttonce.Text = "CE";
            this.buttonce.UseVisualStyleBackColor = true;
            this.buttonce.Click += new System.EventHandler(this.buttonce_Click);
            // 
            // buttonizlaz
            // 
            this.buttonizlaz.Location = new System.Drawing.Point(281, 366);
            this.buttonizlaz.Name = "buttonizlaz";
            this.buttonizlaz.Size = new System.Drawing.Size(136, 58);
            this.buttonizlaz.TabIndex = 22;
            this.buttonizlaz.Text = "Izlaz";
            this.buttonizlaz.UseVisualStyleBackColor = true;
            this.buttonizlaz.Click += new System.EventHandler(this.buttonizlaz_Click);
            // 
            // buttonjednako
            // 
            this.buttonjednako.Location = new System.Drawing.Point(281, 289);
            this.buttonjednako.Name = "buttonjednako";
            this.buttonjednako.Size = new System.Drawing.Size(136, 59);
            this.buttonjednako.TabIndex = 23;
            this.buttonjednako.Text = "=";
            this.buttonjednako.UseVisualStyleBackColor = true;
            this.buttonjednako.Click += new System.EventHandler(this.buttonjednako_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 437);
            this.Controls.Add(this.buttonjednako);
            this.Controls.Add(this.buttonizlaz);
            this.Controls.Add(this.buttonce);
            this.Controls.Add(this.buttontocka);
            this.Controls.Add(this.textbox);
            this.Controls.Add(this.buttonpower);
            this.Controls.Add(this.buttonlog);
            this.Controls.Add(this.buttonsqrt);
            this.Controls.Add(this.buttondijeli);
            this.Controls.Add(this.buttonminus);
            this.Controls.Add(this.buttonmnozi);
            this.Controls.Add(this.buttonsinus);
            this.Controls.Add(this.buttoncosinus);
            this.Controls.Add(this.buttonjedan);
            this.Controls.Add(this.buttondva);
            this.Controls.Add(this.buttontri);
            this.Controls.Add(this.buttoncetiri);
            this.Controls.Add(this.buttonpet);
            this.Controls.Add(this.buttonsest);
            this.Controls.Add(this.buttonsedam);
            this.Controls.Add(this.buttonosam);
            this.Controls.Add(this.buttondevet);
            this.Controls.Add(this.buttonplus);
            this.Controls.Add(this.buttonnula);
            this.Name = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonnula;
        private System.Windows.Forms.Button buttonplus;
        private System.Windows.Forms.Button buttondevet;
        private System.Windows.Forms.Button buttonosam;
        private System.Windows.Forms.Button buttonsedam;
        private System.Windows.Forms.Button buttonsest;
        private System.Windows.Forms.Button buttonpet;
        private System.Windows.Forms.Button buttoncetiri;
        private System.Windows.Forms.Button buttontri;
        private System.Windows.Forms.Button buttondva;
        private System.Windows.Forms.Button buttonjedan;
        private System.Windows.Forms.Button buttoncosinus;
        private System.Windows.Forms.Button buttonsinus;
        private System.Windows.Forms.Button buttonmnozi;
        private System.Windows.Forms.Button buttonminus;
        private System.Windows.Forms.Button buttondijeli;
        private System.Windows.Forms.Button buttonsqrt;
        private System.Windows.Forms.Button buttonlog;
        private System.Windows.Forms.Button buttonpower;
        private System.Windows.Forms.TextBox textbox;
        private System.Windows.Forms.Button buttontocka;
        private System.Windows.Forms.Button buttonce;
        private System.Windows.Forms.Button buttonizlaz;
        private System.Windows.Forms.Button buttonjednako;
    }
}

