﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza_Zadatak1
{
    public partial class Form1 : Form
    {
        double prvi;
        string operacija;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textbox.Text = textbox.Text + "0";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (textbox.Text == "0" && textbox.Text != null)
            {
                textbox.Text = "1";
            }
            else
            {
                textbox.Text = textbox.Text + "1";
            }
        }

        private void buttonplus_Click(object sender, EventArgs e)
        {
            prvi = Convert.ToDouble(textbox.Text);
            textbox.Text = "0";
            operacija = "+";
        }

        private void buttoncetiri_Click(object sender, EventArgs e)
        {
            if (textbox.Text == "0" && textbox.Text != null)
            {
                textbox.Text = "4";
            }
            else
            {
                textbox.Text = textbox.Text + "4";
            }
        }

        private void buttonsest_Click(object sender, EventArgs e)
        {
            if (textbox.Text == "0" && textbox.Text != null)
            {
                textbox.Text = "6";
            }
            else
            {
                textbox.Text = textbox.Text + "6";
            }
        }

        private void buttonminus_Click(object sender, EventArgs e)
        {
            prvi = Convert.ToDouble(textbox.Text);
            textbox.Text = "0";
            operacija = "-";
        }

        private void buttondijeli_Click(object sender, EventArgs e)
        {
            prvi = Convert.ToDouble(textbox.Text);
            textbox.Text = "0";
            operacija = "/";
        }

        private void buttonsqrt_Click(object sender, EventArgs e)
        {
            prvi = Convert.ToDouble(textbox.Text);
            double Result = 0;
            Result = Math.Sqrt(prvi);
            textbox.Text = Result.ToString();
        }

        private void buttondva_Click(object sender, EventArgs e)
        {
            if (textbox.Text == "0" && textbox.Text != null)
            {
                textbox.Text = "2";
            }
            else
            {
                textbox.Text = textbox.Text + "2";
            }
        }

        private void buttontri_Click(object sender, EventArgs e)
        {
            if (textbox.Text == "0" && textbox.Text != null)
            {
                textbox.Text = "3";
            }
            else
            {
                textbox.Text = textbox.Text + "3";
            }
        }

        private void buttonpet_Click(object sender, EventArgs e)
        {
            if (textbox.Text == "0" && textbox.Text != null)
            {
                textbox.Text = "5";
            }
            else
            {
                textbox.Text = textbox.Text + "5";
            }
        }

        private void buttonsedam_Click(object sender, EventArgs e)
        {
            if (textbox.Text == "0" && textbox.Text != null)
            {
                textbox.Text = "7";
            }
            else
            {
                textbox.Text = textbox.Text + "7";
            }
        }

        private void buttonosam_Click(object sender, EventArgs e)
        {
            if (textbox.Text == "0" && textbox.Text != null)
            {
                textbox.Text = "8";
            }
            else
            {
                textbox.Text = textbox.Text + "8";
            }
        }

        private void buttondevet_Click(object sender, EventArgs e)
        {
            if (textbox.Text == "0" && textbox.Text != null)
            {
                textbox.Text = "9";
            }
            else
            {
                textbox.Text = textbox.Text + "9";
            }
        }

        private void buttontocka_Click(object sender, EventArgs e)
        {
            textbox.Text = textbox.Text + ".";
        }

        private void buttonizlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonce_Click(object sender, EventArgs e)
        {
            textbox.Text = "0";
        }

        private void buttonmnozi_Click(object sender, EventArgs e)
        {
            prvi = Convert.ToDouble(textbox.Text);
            textbox.Text = "0";
            operacija = "*";
        }

        private void buttonjednako_Click(object sender, EventArgs e)
        {
            double drugi;
            double rezultat;

            drugi = Convert.ToDouble(textbox.Text);

            if (operacija == "+")
            {
                rezultat = (prvi + drugi);
                textbox.Text = Convert.ToString(rezultat);
                prvi = rezultat;
            }
            if (operacija == "-")
            {
                rezultat = (prvi - drugi);
                textbox.Text = Convert.ToString(rezultat);
                prvi = rezultat;
            }
            if (operacija == "*")
            {
                rezultat = (prvi * drugi);
                textbox.Text = Convert.ToString(rezultat);
                prvi = rezultat;
            }
            if (operacija == "/")
            {
                if (drugi == 0)
                {
                    textbox.Text = "Ne može se dijelit s nulom!";
                }
                else
                {
                    rezultat = (prvi / drugi);
                    textbox.Text = Convert.ToString(rezultat);
                    prvi = rezultat;
                }
            }
        }

        private void buttonsinus_Click(object sender, EventArgs e)
        {
            prvi = Convert.ToDouble(textbox.Text);
            double Result = 0;
            Result = Math.Sin(prvi);
            textbox.Text = Result.ToString();
        }

        private void buttoncosinus_Click(object sender, EventArgs e)
        {
            prvi = Convert.ToDouble(textbox.Text);
            double Result = 0;
            Result = Math.Cos(prvi);
            textbox.Text = Result.ToString();
        }

        private void buttonlog_Click(object sender, EventArgs e)
        {
            prvi = Convert.ToDouble(textbox.Text);
            double Result = 0;
            Result = Math.Log(prvi);
            textbox.Text = Result.ToString();
        }

        private void buttonpower_Click(object sender, EventArgs e)
        {
            prvi = Convert.ToDouble(textbox.Text);
            double Result = 0;
            Result = Math.Pow(prvi,2);
            textbox.Text = Result.ToString();
        }
    }
}
