﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza_Zadatak2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string rijec;
        int broj_zivota = 8;
        int tocno = 0;
        Random rand = new Random();
        List<string> lista = new List<string>();
        string path = "D:\\Programi i driveri\\Analize\\LV6 OBJEKTNO\\Analiza-Zadatak2\\Analiza-Zadatak2\\lista.txt";
        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textbox.Text.Length == 0 || textbox.Text.Length > 1)
            {
                textbox.Text = "";
                MessageBox.Show("Unesite slovo");
            }
            else
            {
                if(rijec.Contains(textbox.Text))
                {
                    for(int i=0; i<rijec.Length; i++)
                    {
                        if(rijec.IndexOf(textbox.Text)==rijec.IndexOf(rijec[i]))
                        {
                            switch(i)
                            {
                                case 0:
                                    if(label2.Text == "_")
                                    {
                                        label2.Text = textbox.Text;
                                        tocno++;
                                 
                                    }
                                    else
                                    {
                                        MessageBox.Show("Unešeno već");
                                        
                                    }
                                    break;
                                case 1:
                                    if (label3.Text == "_")
                                    {
                                        label3.Text = textbox.Text;
                                        tocno++;

                                    }
                                    else
                                    {
                                        MessageBox.Show("Unešeno već");
                                       
                                    }
                                    break;
                                case 2:
                                    if (label4.Text == "_")
                                    {
                                        label4.Text = textbox.Text;
                                        tocno++;

                                    }
                                    else
                                    {
                                        MessageBox.Show("Unešeno već");
                                    }
                                    break;
                                case 3:
                                    if (label5.Text == "_")
                                    {
                                        label5.Text = textbox.Text;
                                        tocno++;

                                    }
                                    else
                                    {
                                        MessageBox.Show("Unešeno već");
                                       
                                    }
                                    break;
                                case 4:
                                    if (label6.Text == "_")
                                    {
                                        label6.Text = textbox.Text;
                                        tocno++;

                                    }
                                    else
                                    {
                                        MessageBox.Show("Unešeno već");
                                        
                                    }
                                    break;
                                case 5:
                                    if (label7.Text == "_")
                                    {
                                        label7.Text = textbox.Text;
                                        tocno++;

                                    }
                                    else
                                    {
                                        MessageBox.Show("Unešeno već");
                                        
                                    }
                                    break;
                                case 6:
                                    if (label8.Text == "_")
                                    {
                                        label8.Text = textbox.Text;
                                        tocno++;

                                    }
                                    else
                                    {
                                        MessageBox.Show("Unešeno već");
                                        
                                    }
                                    break;
                                case 7:
                                    if (label9.Text == "_")
                                    {
                                        label9.Text = textbox.Text;
                                        tocno++;

                                    }
                                    else
                                    {
                                        MessageBox.Show("Unešeno već");
                                        
                                    }
                                    break;
                            }
                        }
                    }

                    if(tocno==rijec.Length)
                    {
                        MessageBox.Show("Točno ste rješili igru!");
                        Application.Exit();
                    }
                }

                else
                {
                    if(broj_zivota==0)
                    {
                        MessageBox.Show("Nažalost niste rješili igru");
                        Application.Exit();
                    }
                    else
                    {
                        broj_zivota--;
                        label10.Text = "Broj života: " + broj_zivota;
                        
                    }
                }
            }

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string unos;
                while ((unos = reader.ReadLine()) != null)
                {
                    lista.Add(unos);
                }
            }

            int rnd = rand.Next(lista.Count);
            rijec = (string)lista[rnd];

            if (rijec.Length == 1)
            {
                label2.Text = "_";
                label3.Text = "";
                label4.Text = "";
                label5.Text = "";
                label6.Text = "";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
            }

            else if (rijec.Length == 2)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "";
                label5.Text = "";
                label6.Text = "";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
            }

            else if (rijec.Length == 3)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "";
                label6.Text = "";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
            }

            else if (rijec.Length == 4)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
            }

            else if (rijec.Length == 5)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "";
                label8.Text = "";
                label9.Text = "";
            }

            else if (rijec.Length == 6)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "_";
                label8.Text = "";
                label9.Text = "";
            }

            else if (rijec.Length == 7)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "_";
                label8.Text = "_";
                label9.Text = "";
            }

            else if (rijec.Length == 8)
            {
                label2.Text = "_";
                label3.Text = "_";
                label4.Text = "_";
                label5.Text = "_";
                label6.Text = "_";
                label7.Text = "_";
                label8.Text = "_";
                label9.Text = "_";
            }
        }
    }
}
